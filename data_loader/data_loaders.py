from torchvision import datasets, transforms
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler

from pathlib import Path

from base import BaseDataLoader
import data_loader.transforms.base as baseT

#########
# MNIST #
#########

from six.moves import urllib
opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
urllib.request.install_opener(opener)

class MnistDataLoader(BaseDataLoader):
    """
    MNIST data loading demo using BaseDataLoader
    """
    def __init__(self, data_dir, batch_size, shuffle=True, validation_split=0.0, num_workers=1, training=True):
        trsfm = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])
        self.data_dir = data_dir
        self.dataset = datasets.MNIST(self.data_dir, train=training, download=True, transform=trsfm)
        super().__init__(self.dataset, batch_size, shuffle, validation_split, num_workers)


##########
# CAMVID #
##########

class_info = ['building', 'tree', 'sky', 'car', 'sign', 'road', 'pedestrian', 'fence', 'column pole', 'sidewalk',
              'bicyclist']
color_info = [(128, 0, 0), (128, 128, 0), (128, 128, 128), (64, 0, 128), (192, 128, 128), (128, 64, 128), (64, 64, 0),
              (64, 74, 128), (192, 192, 128), (0, 0, 192), (0, 128, 192)]

color_info += [[0, 0, 0]]


class CamVid(Dataset):
    class_info = class_info
    color_info = color_info
    num_classes = len(class_info)

    mean = [111.376, 63.110, 83.670]
    std = [41.608, 54.237, 68.889]

    def __init__(self, root: Path, transforms: lambda x: x, subset='train'):
        self.root = root
        self.subset = subset
        self.image_names = [line.rstrip() for line in (root / f'{subset}.txt').open('r').readlines()]
        name_filter = lambda x: x.name in self.image_names
        self.images = list(filter(name_filter, (self.root / 'rgb').iterdir()))
        self.labels = list(filter(name_filter, (self.root / 'labels/ids').iterdir()))
        self.transforms = transforms

    def __len__(self):
        return len(self.images)

    def __getitem__(self, item):
        ret_dict = {
            'image': self.images[item],
            'name': self.images[item].stem,
            'subset': self.subset,
            'labels': self.labels[item]
        }
        return self.transforms(ret_dict)


class CamVidDataLoader(BaseDataLoader):

    def __init__(self, data_dir, batch_size, shuffle=True, validation_split=0.0, num_workers=1, training=True):

        self.transform = baseT.Compose([
            baseT.Open(),
            baseT.Downsample(2),
            baseT.Tensor()
        ])
        self.data_dir = Path(data_dir)
        self.subset = 'train' if training else 'test'
        self.dataset = CamVid(self.data_dir, self.transform, self.subset)
        super().__init__(self.dataset, batch_size, shuffle, validation_split, num_workers)

    def split_validation(self):
        self.valid_dataset = CamVid(self.data_dir, self.transform, 'val')
        return DataLoader(self.valid_dataset, batch_size=self.batch_size, shuffle=False)

    def _split_sampler(self, split):
        self.shuffle = False
        return SubsetRandomSampler(range(self.n_samples)), None
