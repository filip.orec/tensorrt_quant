import data_loader.cityscapes.labels as l
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np

labels = l.labels

good_labels = [lab for lab in labels if lab.trainId != 255 and lab.trainId != -1]

mycmap = np.ones((20, 4), dtype=np.float32)
cmap = ListedColormap(mycmap)
for label in good_labels:
    print(label.trainId)
    color = np.array((*label.color, 255), dtype=np.float32) / 255.
    print(color)
    mycmap[label.trainId] = color
mycmap[19] = np.array([0, 0, 0, 1], dtype=np.float32)

legend_data = []
for label in good_labels:
    val = label.trainId
    color = [*label.color]
    name = label.name
    legend_data.append([val, color, name])

legend_data.append([19, [0, 0, 0], 'unlabeled'])
print(legend_data)
for k,c,n in legend_data:
    print(k, c, n)
handles = [
    Rectangle((0, 0), 1, 1, color=tuple((v / 255 for v in c))) for k,c,n in legend_data
]
labels = [n for k,c,n in legend_data]
plt.legend(handles, labels, mode='expand', ncol=3)
plt.axis('off')
plt.show()
