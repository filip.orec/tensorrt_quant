import torch
import torch.onnx as onnx

import model.model as m

mean = [73.15, 82.90, 72.3]
std = [47.67, 48.49, 47.73]

if __name__ == '__main__':
    model_single_scale = m.rn18_single_scale(pretrained=False, num_classes=19, image_size=(1024, 2048), mean=mean, std=std)
#    checkpoint = torch.load(
#        "/home/johnsm1th/FER/tensorRT_quant/saved/models/CamVid_SwiftNet/0309_200621/checkpoint-epoch250.pth")
    model_single_scale.load_state_dict(torch.load('swiftnet_ss_cs_2.pt'))
    model_single_scale.to('cuda')
    model_single_scale.eval()
    input_ = torch.ones((1, 3, 1024, 2048)).to('cuda')
    model_single_scale.forward(input_)
    onnx.export(model_single_scale, input_, 'swiftnet_argmax_cityscapes.onnx', opset_version=11, verbose=True, do_constant_folding=True, export_params=True,
                input_names=["input"], output_names=["pred"]) # , "spp_out", "RB2_out", "RB1_out", "RB0_out", "upsample0_out", "upsample1_out", "upsample2_out"])
