#ifndef STB_IMAGE_WRITE_IMPLEMENTATION
    #define STB_IMAGE_WRITE_IMPLEMENTATION
    #include <lib/stb/stb_image_write.h>
#endif

#include <NvInferPlugin.h>
#include <NvInferRuntime.h>
#include <NvInferRuntimeCommon.h>

#include <chrono>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <memory>
#include <stdio.h>
#include <string>
#include <time.h>
#include <cudnn.h>

#include "NvInfer.h"
#include "NvOnnxParser.h"

#include "common.h"
#include "logger.h"
#include "buffers.h"
#include "argsParser.h"
#include "BatchStream.h"
#include "EntropyCalibrator.h"


const std::string gSampleName = "TensorRT.swiftnet";
const std::string jetson_cs_path = "/home/forec/tensorRT_quant/data/Cityscapes/orig";
const std::string jetson_result_dir = "/home/forec/tensorRT_quant/inference/results/";

//void* buffers[2];
//void** io_buffers;
//float* input;

struct SwiftNetParams : public samplesCommon::OnnxSampleParams {
    int nbCalBatches;           //!< The nuber of bathces for calibration
    int calBatchSize;           //!< The calibration batch size
    std::string networkName;
};

class SwiftNetEngine {
    template<typename T>
    using SNUniquePtr = std::unique_ptr<T, samplesCommon::InferDeleter>;

    public:
        SwiftNetEngine(const SwiftNetParams& params)
            : mParams(params)
            , mEngine(nullptr)
        {
            initLibNvInferPlugins(&sample::gLogger.getTRTLogger(), "");
        }

        //!
        //! \brief Function builds the network engine
        //!
        bool build(DataType dataType);

        //! \brief Checks if the platform supports the data type.
        //!
        //! \return Returns true if the platform supports the data type.
        //!
        bool isSupported(DataType dataType);

        //!
        //! \brief Runs the TensorRT inference engine for this sample
        //!
        bool infer();

        //!
        //! \brief Cleans up any state created in the sample class
        //!
        bool teardown();

        bool saveEngine(const char* filepath);
        bool loadEngine(const char* filepath);
        bool performanceTest();
        bool accuracyTest();

    private:
        SwiftNetParams mParams;

        nvinfer1::Dims mInputDims;
        nvinfer1::Dims mOutputDims;

        std::shared_ptr<nvinfer1::ICudaEngine> mEngine;
        //!
        //! \brief Parses a ONNX model and creates a TensorRT network
        //!
        bool constructNetwork(SNUniquePtr<nvinfer1::IBuilder>& builder,
            SNUniquePtr<nvinfer1::INetworkDefinition>& network, SNUniquePtr<nvinfer1::IBuilderConfig>& config,
            SNUniquePtr<nvonnxparser::IParser>& parser, DataType data_type);

        //!
        //! \brief Moves data to a managed buffer
        //!
        bool moveToInputBuffer(const samplesCommon::BufferManager& buffers, float* data);

        //!
        //! \brief Creates random input and stores it in a managed buffer
        //!
        bool populateRandom(const samplesCommon::BufferManager& buffers);

        void setLayerPrecision(SNUniquePtr<nvinfer1::INetworkDefinition>& network);

        //!
        //! \brief Scores model
        //!
        int calculateScore(
        const samplesCommon::BufferManager& buffers, float* labels, int batchSize, int outputSize, int threshold);
};

void SwiftNetEngine::setLayerPrecision(SNUniquePtr<nvinfer1::INetworkDefinition>& network) {
    int n = network->getNbLayers();
//    int32_t i = 46;     // start of the upsampling part (spp)
    for(int i = 0; i < n; ++i) {
        auto layer = network->getLayer(i);

        std::string name = layer->getName();

        auto layer_type = layer->getType();
        if(layer_type != LayerType::kCONSTANT && layer_type != LayerType::kCONCATENATION
                && layer_type != LayerType::kSHAPE && layer_type != LayerType::kGATHER &&
                layer_type != LayerType::kSHUFFLE && layer_type != LayerType::kIDENTITY
                && layer_type != LayerType::kSLICE) {
            if (i <= 56) {
                layer->setPrecision(DataType::kINT8);
            } else {
                if(layer_type == LayerType::kRESIZE) {
                    layer->setPrecision(DataType::kHALF);
                } else {
                    layer->setPrecision(DataType::kINT8);
                }
            }
        }
        std::string precision;
        switch((int)layer->getPrecision()) {
            case 0:
                precision = "FP32";
                break;
            case 1:
                precision = "FP16";
                break;
            case 2:
                precision = "INT8";
                break;
            case 3:
                precision = "INT32";
                break;
            default:
                precision = "BOOL";
                break;

        }
        sample::gLogInfo << "Layer " << std::to_string(i) << ": " << name << ". Precision: " << precision << std::endl;

        // layer 56 je prvi sloj od spp-a
        for(int j = 0, o = layer->getNbOutputs(); j < o; ++j) {

//            if (layer_type != LayerType::kSHAPE) {
            auto layer_output = layer->getOutput(j);
            std::string tensor_name = layer_output->getName();
            if(layer_type != LayerType::kCONSTANT && layer_type != LayerType::kCONCATENATION
                && layer_type != LayerType::kSHAPE && layer_type != LayerType::kGATHER &&
                layer_type != LayerType::kSHUFFLE && layer_type != LayerType::kIDENTITY
                && layer_type != LayerType::kSLICE) {
                if (i > 56) {
                    if (name == "Conv_191" || name == "Conv_166" || name == "Conv_140"
                            || name == "Conv_114" || name == "Conv_77" || name == "Conv_60") {
                        layer->setOutputType(j, DataType::kHALF);

                    } else {
                        layer->setOutputType(j, DataType::kINT8);
                    }
                }
                if(layer_type == LayerType::kRESIZE) {
                    layer->setOutputType(j, DataType::kHALF);
                }
            }
            else {
                layer->setOutputType(j, DataType::kINT32);
            }

            int output_type = (int)layer->getOutputType(j);
            switch((int)output_type) {
                case 0:
                    precision = "FP32";
                    break;
                case 1:
                    precision = "FP16";
                    break;
                case 2:
                    precision = "INT8";
                    break;
                case 3:
                    precision = "INT32";
                    break;
                default:
                    precision = "BOOL";
                    break;

            }
            sample::gLogInfo << "Tensor: " << tensor_name << ". Output type: " << precision << std::endl;
        }
    }
}


bool SwiftNetEngine::saveEngine(const char* filepath) {
    IHostMemory *serializedModel = mEngine->serialize();

    if (!serializedModel) {
        sample::gLogError << "Failed to serialize CUDA engine" << std::endl;
        return false;
    }

    std::ofstream ofs(filepath, std::ios::out | std::ios::binary);
    ofs.write((char*)serializedModel->data(), serializedModel->size());
    ofs.close();
    serializedModel->destroy();
    return true;
}

bool SwiftNetEngine::loadEngine(const char* filepath) {
    std::ifstream ifs(filepath, std::ios::in | std::ios::binary);
    if (!ifs.is_open()) {
        sample::gLogError << "Failed to open engine file" << std::endl;
        return false;
    }

    // read engine size
    ifs.seekg(0, std::ios::end);
    size_t engine_size = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    // read engine data
    std::unique_ptr<char[]> engine_data = std::make_unique<char[]>(engine_size);
    ifs.read(engine_data.get(), engine_size);
    ifs.close();

    // deserialize engine
    nvinfer1::IRuntime* infer = nvinfer1::createInferRuntime(sample::gLogger);
    if (!infer) {
        sample::gLogError << "Failed to create InferRuntime" << std::endl;
        return false;
    }
    mEngine = std::shared_ptr<nvinfer1::ICudaEngine>(
                infer->deserializeCudaEngine(engine_data.get(), engine_size, nullptr),
                samplesCommon::InferDeleter()
            );
    if (!mEngine.get()) {
        sample::gLogError << "Failed to create CUDA engine" << std::endl;
        return false;
    }

    mInputDims = mEngine->getBindingDimensions(mEngine->getBindingIndex("input"));
    mOutputDims = mEngine->getBindingDimensions(mEngine->getBindingIndex(mParams.outputTensorNames[0].c_str()));
    printf("Input dims: [%d, %d, %d, %d]\n", mInputDims.d[0], mInputDims.d[1], mInputDims.d[2], mInputDims.d[3]);
    printf("Output dims: [%d, %d, %d, %d]\n", mOutputDims.d[0], mOutputDims.d[1], mOutputDims.d[2], mOutputDims.d[3]);

    return true;
}

bool SwiftNetEngine::build(DataType data_type) {

    auto builder = SNUniquePtr<nvinfer1::IBuilder>(nvinfer1::createInferBuilder(sample::gLogger.getTRTLogger()));
    if (!builder) {
        return false;
    }

    const auto explicit_batch = 1U << static_cast<uint32_t>(NetworkDefinitionCreationFlag::kEXPLICIT_BATCH);
    auto network = SNUniquePtr<nvinfer1::INetworkDefinition>(builder->createNetworkV2(explicit_batch));
    if (!network) {
        return false;
    }

    auto config = SNUniquePtr<nvinfer1::IBuilderConfig>(builder->createBuilderConfig());
    if (!config) {
        return false;
    }
    config->setMaxWorkspaceSize(25_GiB);

    auto parser = SNUniquePtr<nvonnxparser::IParser>(nvonnxparser::createParser(*network, sample::gLogger.getTRTLogger()));
    if (!parser) {
        std::string msg("Failed to parse ONNX file.");
        sample::gLogger.log(nvinfer1::ILogger::Severity::kERROR, msg.c_str());
        return false;
    }

    auto constructed = constructNetwork(builder, network, config, parser, data_type);
    if (!constructed) {
        std::string msg("failed to parse onnx file");
        sample::gLogger.log(nvinfer1::ILogger::Severity::kERROR, msg.c_str());
        exit(EXIT_FAILURE);
    }

    assert(network->getNbInputs() == 1);
    mInputDims = mEngine->getBindingDimensions(mEngine->getBindingIndex("input"));
    printf("Input dims: [%d %d %d %d]\n", mInputDims.d[0], mInputDims.d[1],mInputDims.d[2],mInputDims.d[3]);
    assert(mInputDims.nbDims == 4);

    assert(network->getNbOutputs() == 1);
    mOutputDims = mEngine->getBindingDimensions(mEngine->getBindingIndex(mParams.outputTensorNames[0].c_str()));
    printf("Output dims: [%d %d %d %d]\n", mOutputDims.d[0], mOutputDims.d[1],mOutputDims.d[2],mOutputDims.d[3]);

    // we don't need these objects anymore
    network.release()->destroy();
    parser.release()->destroy();
    builder.release()->destroy();
    config.release()->destroy();

    return true;
}

bool SwiftNetEngine::constructNetwork(SNUniquePtr<nvinfer1::IBuilder>& builder,
        SNUniquePtr<nvinfer1::INetworkDefinition>& network, SNUniquePtr<nvinfer1::IBuilderConfig>& config,
        SNUniquePtr<nvonnxparser::IParser>& parser, DataType data_type) {

    auto parsed = parser->parseFromFile(locateFile(mParams.onnxFileName, mParams.dataDirs).c_str(),
            static_cast<int>(sample::gLogger.getReportableSeverity()));

    std::unique_ptr<nvinfer1::IInt8Calibrator> calibrator;
    if (!parsed) {
        return false;
    }
    if (mParams.fp16) {
        sample::gLogInfo << "Building engine in FP16 mode!" << std::endl;
        config->setFlag(nvinfer1::BuilderFlag::kFP16);
    }
    if (mParams.int8) {
        sample::gLogInfo << "Building engine in INT8 mode!" << std::endl;
        config->setFlag(nvinfer1::BuilderFlag::kINT8);
        config->setFlag(nvinfer1::BuilderFlag::kFP16);
        setLayerPrecision(network);

        CityScapeBatchStream calibration_stream(1, 10, jetson_cs_path, "cal");
        calibrator.reset(new Int8EntropyCalibrator2<CityScapeBatchStream>(
                    calibration_stream, 0, "SwiftNet", mParams.inputTensorNames[0].c_str()));
        config->setInt8Calibrator(calibrator.get());

    }

    sample::gLogInfo << "Building CUDA engine" << std::endl;

    mEngine = std::shared_ptr<nvinfer1::ICudaEngine>(
            builder->buildEngineWithConfig(*network, *config), samplesCommon::InferDeleter()
            );
    if(!mEngine) {
        std::string msg("failed to build cuda engine");
        sample::gLogger.log(nvinfer1::ILogger::Severity::kERROR, msg.c_str());
        exit(EXIT_FAILURE);
    }

    sample::gLogInfo << "Completed building CUDA engine" << std::endl;
    return true;
}

SwiftNetParams initSwiftNetParams(const samplesCommon::Args& args) {
    SwiftNetParams params;
    if (args.dataDirs.empty()) {
        params.dataDirs.push_back("data/camvid/");
        params.dataDirs.push_back("data/cityscapes");

    } else {
        params.dataDirs = args.dataDirs;
    }
    params.onnxFileName = "swiftnet_logits_cityscapes.onnx";
    params.networkName = "swiftnet";
    params.inputTensorNames.push_back("input");
    params.outputTensorNames.push_back("logits");
    params.int8 = args.runInInt8;
    params.fp16 = args.runInFp16;

    return params;
}

bool SwiftNetEngine::infer() {

    CityScapeBatchStream batch_stream(1, 1, jetson_cs_path, "val");

    auto context = SNUniquePtr<nvinfer1::IExecutionContext>(mEngine->createExecutionContext());
    if (!context) {
        sample::gLogError << "Failed to create execution context" << std::endl;
        return false;
    }

//    input = batch_stream.getBatch();
//
//    cudaStream_t stream;
//    CHECK(cudaStreamCreate(&stream));
//    CHECK(cudaMalloc(&buffers[0], 1024*2048*3*sizeof(float)));
//    CHECK(cudaMalloc(&buffers[1], 1024*2048*sizeof(int)));
//    CHECK(cudaMemcpyAsync(buffers[0], input, 1024 * 2048 * 3 * sizeof(float), cudaMemcpyHostToDevice, stream));
//
//    context->enqueue(1, buffers, stream, nullptr);
//
//    int* output = new int[19*1024*2048];
//    CHECK(cudaMemcpyAsync(buffers[1], output, 1024 * 2048 * sizeof(int), cudaMemcpyDeviceToHost, stream));
//    cudaStreamSynchronize(stream);
//    cudaStreamDestroy(stream);
//    CHECK(cudaFree(buffers[0]));
//    CHECK(cudaFree(buffers[1]));
//
//    delete[] input;
//    delete[] output;

    samplesCommon::BufferManager buffers(mEngine);

    float* input = batch_stream.getBatch();
    if(!moveToInputBuffer(buffers, batch_stream.getBatch())) {
        return false;
    }
    float* hostDataBuffer = static_cast<float*>(buffers.getHostBuffer(mParams.inputTensorNames[0]));
    buffers.copyInputToDevice();

    bool status = context->executeV2(buffers.getDeviceBindings().data());
    if(!status) {
        return false;
    }

    buffers.copyOutputToHost();
    int* output = (int*)buffers.getHostBuffer(mParams.outputTensorNames[0]);
    samplesCommon::write_semseg_map_as_text<1024, 2048>("test.txt", output);

    delete[] input;
    delete[] output;

    return true;
}

bool SwiftNetEngine::performanceTest() {
    samplesCommon::BufferManager buffers(mEngine);

    auto context = SNUniquePtr<nvinfer1::IExecutionContext>(mEngine->createExecutionContext());
    if (!context) {
        sample::gLogError << "Failed to create execution context" << std::endl;
        return false;
    }


    // Init
    for(int i = 0; i < 200; ++i) {
        this->populateRandom(buffers);
        buffers.copyInputToDevice();
        context->executeV2(buffers.getDeviceBindings().data());
    }

    float time_avg = 0.0f;
    for(int i = 0; i < 100; ++i) {
        this->populateRandom(buffers);
        buffers.copyInputToDevice();
        auto data = buffers.getDeviceBindings().data();
        auto t1 = std::chrono::high_resolution_clock::now();
        context->executeV2(data);
        auto t2 = std::chrono::high_resolution_clock::now();
        float inference_time = ((float) std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count());
        buffers.copyOutputToHost();
        time_avg += inference_time;
        printf("Inference time: %.3f ms.\n", inference_time);
    }
    sample::gLogInfo << "Avg inference: " << time_avg / 100 << " ms." << std::endl;

    return true;
}

void add_confusion_matrix(int* y, uint8_t* yt, uint32_t n, uint64_t* matrix, uint32_t num_classes) {
    for(uint32_t i = 0; i < n; ++i) {
        uint8_t target = yt[i];
        if(target >= 0 && target < num_classes) {
            matrix[y[i] * num_classes + target] += 1;
        }
    }
}

bool SwiftNetEngine::accuracyTest() {
    samplesCommon::BufferManager buffers(mEngine);

    auto context = SNUniquePtr<nvinfer1::IExecutionContext>(mEngine->createExecutionContext());
    if (!context) {
        sample::gLogError << "Failed to create execution context" << std::endl;
        return false;
    }

    CityScapeBatchStream batch_stream(1, 500, jetson_cs_path, "val");

    int cnt = 0;

    do {
        printf("%d\n", cnt);

        // Data loading
        float* input = batch_stream.getBatch();     // load input image
        this->moveToInputBuffer(buffers, input);    // copy loaded image to input buffer
        buffers.copyInputToDevice();                // copy input buffer to GPU

        // Inference
        context->executeV2(buffers.getDeviceBindings().data());     // Infer

        // Getting output
        buffers.copyOutputToHost();
        int* output = static_cast<int*>(buffers.getHostBuffer(mParams.outputTensorNames[0]));

        samplesCommon::write_semseg_map_as_bin<1024, 2048>(jetson_result_dir + std::to_string(cnt) + ".out", output);
        ++cnt;

    } while(batch_stream.next());

    return true;
}

bool SwiftNetEngine::moveToInputBuffer(const samplesCommon::BufferManager& buffers, float* data) {
    const int vol = samplesCommon::volume(this->mInputDims);
    float* hostDataBuffer = static_cast<float*>(buffers.getHostBuffer(mParams.inputTensorNames[0]));
    std::memcpy(hostDataBuffer, data, vol * sizeof(float));
    delete[] data;
    return true;
}

bool SwiftNetEngine::populateRandom(const samplesCommon::BufferManager& buffers) {
    const int vol = samplesCommon::volume(this->mInputDims);
    float* hostDataBuffer = static_cast<float*>(buffers.getHostBuffer(mParams.inputTensorNames[0]));
    for(int i = 0; i < vol; ++i) {
        hostDataBuffer[i] = (float)(rand() % 255);
    }
    return true;
}

int main(int argc, char** argv) {
    samplesCommon::Args args;
    bool args_ok = samplesCommon::parseArgs(args, argc, argv);
    if (!args_ok) {
        sample::gLogError << "Invalid arguments" << std::endl;
        return EXIT_FAILURE;
    }
    SwiftNetEngine engine(initSwiftNetParams(args));
    bool ret;
    if (!args.loadEngine.empty()) {
        sample::gLogInfo << "Loading engine from " << args.loadEngine << "" << std::endl;
        ret = engine.loadEngine(args.loadEngine.c_str());
        sample::gLogInfo << "Engine loaded" << std::endl;
    } else {
        ret = engine.build(DataType::kFLOAT);
        if(!args.saveEngine.empty() && ret) {
            engine.saveEngine(args.saveEngine.c_str());
        }
    }
    engine.performanceTest();
//    engine.accuracyTest();
    return 0;
}
