import numpy as np
import data_loader.cityscapes.cityscapes as cs
import matplotlib.pyplot as plt
import lib.cylib as cylib


__all__ = ['compute_errors']


def compute_errors(conf_mat, class_info, verbose=True):
    num_correct = conf_mat.trace()
    num_classes = conf_mat.shape[0]
    total_size = conf_mat.sum()
    avg_pixel_acc = num_correct / total_size * 100.0
    TPFP = conf_mat.sum(1)
    TPFN = conf_mat.sum(0)
    FN = TPFN - conf_mat.diagonal()
    FP = TPFP - conf_mat.diagonal()
    class_iou = np.zeros(num_classes)
    class_recall = np.zeros(num_classes)
    class_precision = np.zeros(num_classes)
    per_class_iou = []
    if verbose:
        print('Errors:')
    for i in range(num_classes):
        TP = conf_mat[i, i]
        class_iou[i] = (TP / (TP + FP[i] + FN[i])) * 100.0
        if TPFN[i] > 0:
            class_recall[i] = (TP / TPFN[i]) * 100.0
        else:
            class_recall[i] = 0
        if TPFP[i] > 0:
            class_precision[i] = (TP / TPFP[i]) * 100.0
        else:
            class_precision[i] = 0

        class_name = class_info[i]
        per_class_iou += [(class_name, class_iou[i])]
        if verbose:
            print('\t%s IoU accuracy = %.2f %%' % (class_name, class_iou[i]))
    avg_class_iou = class_iou.mean()
    avg_class_recall = class_recall.mean()
    avg_class_precision = class_precision.mean()
    if verbose:
        print('IoU mean class accuracy -> TP / (TP+FN+FP) = %.2f %%' % avg_class_iou)
        print('mean class recall -> TP / (TP+FN) = %.2f %%' % avg_class_recall)
        print('mean class precision -> TP / (TP+FP) = %.2f %%' % avg_class_precision)
        print('pixel accuracy = %.2f %%' % avg_pixel_acc)
    return avg_pixel_acc, avg_class_iou, avg_class_recall, avg_class_precision, total_size, per_class_iou


if __name__ == "__main__":
    data_dir = "/home/johnsm1th/FER/tensorRT_quant/data/Cityscapes/full"
    labels_file_names = [line.rstrip() for line in open(data_dir + "/labels_val_paths.txt", "r")]

    conf_mat = np.zeros((19, 19), dtype=np.uint64)

    for i, label_file_name in enumerate(labels_file_names):
        pred = np.fromfile("/home/johnsm1th/FER/tensorRT_quant/inference/results/" + str(i) + ".out", dtype=np.uint8).astype(np.uint32)
        pred = np.reshape(pred, (1024, 2048))
        label = (plt.imread(data_dir + label_file_name) * 255).astype(np.uint32)
        cylib.collect_confusion_matrix(pred.flatten(), label.flatten(), conf_mat)

    res = compute_errors(conf_mat, cs.class_info)
