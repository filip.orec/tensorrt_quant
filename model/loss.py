from torch.nn import functional as F
from torch import nn as nn

from model.util import upsample


def nll_loss(output, target):
    return F.nll_loss(output, target)


class SemsegCrossEntropy(nn.Module):
    def __init__(self, num_classes=19, ignore_id=19):
        super(SemsegCrossEntropy, self).__init__()
        self.num_classes = num_classes
        self.ignore_id = ignore_id

    def loss(self, y, t):
        if y.shape[2:4] != t.shape[1:3]:
            y = upsample(y, t.shape[1:3])
        return F.cross_entropy(y, target=t, ignore_index=self.ignore_id)

    def forward(self, logits, labels, **kwargs):
        loss = self.loss(logits, labels)
        return loss
