import torch
import torch.nn as nn
import torch.nn.functional as F
from itertools import chain
import warnings

from base import BaseModel
from .util import _BNReluConv, upsample
import model.resnet.resnet_single_scale as rnss
import model.resnet.resnet_pyramid as rnpyr


class MnistModel(BaseModel):
    def __init__(self, num_classes=10):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, num_classes)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


class SemsegModel(BaseModel):
    def __init__(self, backbone, num_classes, num_inst_classes=None, use_bn=True, k=1, bias=True,
                 loss_ret_additional=False, upsample_logits=True, logit_class=_BNReluConv,
                 multiscale_factors=(.5, .75, 1.5, 2.), image_size=(360, 480)):
        super().__init__()
        self.image_size= image_size
        self.backbone = backbone
        self.num_classes = num_classes
        self.logits = logit_class(self.backbone.num_features, self.num_classes, batch_norm=use_bn, k=k, bias=bias)
        if num_inst_classes is not None:
            self.border_logits = _BNReluConv(self.backbone.num_features, num_inst_classes, batch_norm=use_bn,
                                             k=k, bias=bias)
        self.criterion = None
        self.loss_ret_additional = loss_ret_additional
        self.img_req_grad = loss_ret_additional
        self.upsample_logits = upsample_logits
        self.multiscale_factors = multiscale_factors

    def forward(self, image):
        features, additional = self.backbone(image)
        logits = self.logits.forward(features)
        if (not self.training) or self.upsample_logits:
            logits = upsample(logits, self.image_size)
        if hasattr(self, 'border_logits'):
            additional['border_logits'] = self.border_logits(features).sigmoid()
        # additional['logits'] = logits
        pred = torch.argmax(logits, dim=1)
        return pred, additional

    def forward_down(self, image, target_size, image_size):
        return self.backbone.forward_down(image), target_size, image_size

    def forward_up(self, feats, target_size, image_size):
        feats, additional = self.backbone.forward_up(feats)
        features = upsample(feats, target_size)
        logits = self.logits.forward(features)
        logits = upsample(logits, image_size)
        return logits, additional

    def prepare_data(self, batch, image_size, device=torch.device('cuda'), img_key='image'):
        if image_size is None:
            image_size = batch['target_size']
        warnings.warn(f'Image requires grad: {self.img_req_grad}', UserWarning)
        image = batch[img_key].detach().requires_grad_(self.img_req_grad).cpu()
        return {
            'image': image,
            'image_size': image_size,
            'target_size': batch.get('target_size_feats')
        }

    def do_forward(self, batch, image_size=None):
        data = self.prepare_data(batch, image_size)
        logits, additional = self.forward(**data)
        additional['model'] = self
        additional = {**additional, **data}
        return logits, additional

    def loss(self, batch):
        assert self.criterion is not None
        labels = batch['labels'].cuda()
        logits, additional = self.do_forward(batch, image_size=labels.shape[-2:])
        if self.loss_ret_additional:
            return self.criterion(logits, labels, batch=batch, additional=additional), additional
        return self.criterion(logits, labels, batch=batch, additional=additional)

    def random_init_params(self):
        params = [self.logits.parameters(), self.backbone.random_init_params()]
        if hasattr(self, 'border_logits'):
            params += [self.border_logits.parameters()]
        return chain(*(params))

    def fine_tune_params(self):
        return self.backbone.fine_tune_params()

    def ms_forward(self, batch, image_size=None):
        image_size = batch.get('target_size', image_size if image_size is not None else batch['image'].shape[-2:])
        ms_logits = None
        pyramid = [batch['image'].cuda()]
        pyramid += [
            F.interpolate(pyramid[0], scale_factor=sf, mode=self.backbone.pyramid_subsample,
                          align_corners=self.backbone.align_corners) for sf in self.multiscale_factors
        ]
        for image in pyramid:
            batch['image'] = image
            logits, additional = self.do_forward(batch, image_size=image_size)
            if ms_logits is None:
                ms_logits = torch.zeros(logits.size()).to(logits.device)
            ms_logits += F.softmax(logits, dim=1)
        batch['image'] = pyramid[0].cpu()
        return ms_logits / len(pyramid), {}


def rn18_single_scale(pretrained=True, efficient=False, mean=[111.376, 63.110, 83.670],
                      std=[41.608, 54.237, 68.889], num_classes=10, ignore_id=-1, image_size=(1024, 2048)):
    resnet = rnss.resnet18(pretrained=pretrained, efficient=efficient, mean=mean, std=std)
    return SemsegModel(resnet, num_classes=num_classes, image_size=image_size)

def rn34_single_scale(pretrained=True, efficient=False, mean=[111.376, 63.110, 83.670],
                      std=[41.608, 54.237, 68.889], num_classes=19, ignore_id=-1, image_size=(1024, 2048)):
    resnet = rnss.resnet34(pretrained=pretrained, efficient=efficient, mean=mean, std=std)
    return SemsegModel(resnet, num_classes=num_classes, image_size=image_size)

def rn18_pyramid(pretrained=True, efficient=False, mean=[73.15, 82.90, 72.3],
                 std=[47.67, 48.49, 47.73], num_classes=19, ignore_id=-1, image_size=(1024, 2048)):
    resnet = rnpyr.resnet18(pretrained=pretrained, efficient=efficient, mean=mean, std=std)
    return SemsegModel(resnet, num_classes=num_classes, image_size=image_size)
